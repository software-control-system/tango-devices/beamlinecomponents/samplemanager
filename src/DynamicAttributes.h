//=============================================================================
// DynamicAttributes.h
//=============================================================================
// abstraction.......Sample Manager Device
// class.............DynamicAttribute
// original author...N.Leclercq - SOLEIL
//=============================================================================

#ifndef _SMD_DYNAMIC_ATTRS_H_
#define _SMD_DYNAMIC_ATTRS_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>


namespace smd
{

	// ============================================================================
	// FORWARD DECLARATIONs
	// ============================================================================
	class SampleManager;

	// ============================================================================
	// Class DynamicAttribute
	// ============================================================================
	class DynamicAttribute : public Tango::LogAdapter
	{
	protected:
		//- ctor
		DynamicAttribute (Tango::DeviceImpl * host);

		//- ctor
		virtual ~DynamicAttribute ();  

	private:
		//- not allowed method and operators
		DynamicAttribute (const DynamicAttribute&);
		const DynamicAttribute & operator= (const DynamicAttribute &);
	};

	// ============================================================================
	// class: DynamicPositionAttribute
	// ============================================================================

	class DynamicPositionAttribute : public Tango::Attr, 
		public Tango::LogAdapter
	{
	public:

		//- ctor argument
		struct Config
		{
			std::string name;
			std::string m_device_name;
			std::string m_attr_name;
			Tango::AttrWriteType m_rw;
			std::string label;
			std::string description;
			Tango::DeviceImpl * m_host;

			//- Ctor -------------
			Config ();

			//- Ctor -------------
			Config ( const Config & _src);

			//- operator = -------
			void operator = (const Config & src);
		};

		//- ctor
		DynamicPositionAttribute (const Config & cfg);

		//- dtor
		virtual ~DynamicPositionAttribute ();

		//- read
		virtual void read(Tango::DeviceImpl *dev, Tango::Attribute &att);

		//- write
		virtual void write(Tango::DeviceImpl *dev, Tango::WAttribute &att);

		//- is_allowed
		virtual bool is_allowed (Tango::DeviceImpl *dev, Tango::AttReqType ty)
		{
			return true;
		}

	private:
		//- the Tango Device Proxy on position
		Tango::DeviceProxy * dp;
		Config conf;

		//- not allowed method and oparators
		DynamicPositionAttribute ();
		DynamicPositionAttribute (const DynamicPositionAttribute&);
		const DynamicPositionAttribute& operator= (const DynamicPositionAttribute&);

	};


} // namespace smid

#endif //- _SMD_DYNAMIC_ATTRS_H_

