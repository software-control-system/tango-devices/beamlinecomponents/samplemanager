//=============================================================================
// DynamicAttributes.cpp
//=============================================================================
// abstraction.......Sample Manager Device
// class.............DynamicAttribute
// original author...N.Leclercq - SOLEIL
//=============================================================================

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// THIS IS NOT USED - THIS IS NOT USED - THIS IS NOT USED - THIS IS NOT USED 
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "DynamicAttributes.h"
#include <math.h>

namespace smd
{

	// ======================================================================
	// Defines and Const definitions
	// ======================================================================
	static const double __NAN__ = ::sqrt (-1.);

	// ======================================================================
	// DynamicAttribute::DynamicAttribute
	// ======================================================================
	DynamicAttribute::DynamicAttribute (Tango::DeviceImpl * host)
		: Tango::LogAdapter(host)
	{
		//- noop
	}

	// ======================================================================
	// DynamicAttribute::~DynamicAttribute
	// ======================================================================
	DynamicAttribute::~DynamicAttribute ()
	{
		//- noop
	}

	// ======================================================================
	// DynamicPositionAttribute::DynamicPositionAttribute
	// ======================================================================
	DynamicPositionAttribute::DynamicPositionAttribute (const Config& _cfg)
		: Tango::LogAdapter (_cfg.m_host),
		Tango::Attr (_cfg.name.c_str (), Tango::DEV_DOUBLE, _cfg.m_rw),
		conf (_cfg),
		dp (0)
	{
		DEBUG_STREAM << "DynamicPositionAttribute::DynamicPositionAttribute  <-" << std::endl;
		Tango::UserDefaultAttrProp	n_aut_mode_prop;
		n_aut_mode_prop.set_label(conf.name.c_str ());
		n_aut_mode_prop.set_format("%6.3f");
		this->set_default_properties(n_aut_mode_prop);

		try
		{
			DEBUG_STREAM << "DynamicPositionAttribute::DynamicPositionAttribute  try to get deviceproxy on " << conf.m_device_name << std::endl;
			dp = new Tango::DeviceProxy (conf.m_device_name);
			if (dp == 0)
				throw (std::bad_alloc ());
		}
		catch (std::bad_alloc)
		{
			ERROR_STREAM << "DynamicPositionAttribute::DynamicPositionAttribute  caught bad_alloc"<< std::endl;
			Tango::Except::throw_exception(static_cast <char *> ("MEMORY_ERROR"),
				static_cast <char *> ("device proxy allocation failed (bad_alloc)!"),
				static_cast <char *> ("DynamicPositionAttribute::DynamicPositionAttribute"));
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << "DynamicPositionAttribute::DynamicPositionAttribute  caught DevFailed " << e << std::endl;
			throw (e);
		}
		catch (...)
		{
			ERROR_STREAM << "DynamicPositionAttribute::DynamicPositionAttribute  caught (...)"<< std::endl;
			Tango::Except::throw_exception(static_cast <char *> ("MEMORY_ERROR"),
				static_cast <char *> ("device proxy allocation failed (...)!"),
				static_cast <char *> ("DynamicPositionAttribute::DynamicPositionAttribute"));
		}
		//- try to ping the device to be sure it is alive
		// 
		// AB : A Quoi cela sert de faire le ping ? 
		dp->ping ();

	}

	// ======================================================================
	// DynamicPositionAttribute::~DynamicPositionAttribute
	// ======================================================================
	DynamicPositionAttribute::~DynamicPositionAttribute ()
	{
		//- delete device proxy
		if (dp)
		{
			delete dp;
			dp = 0;
		}

	}

	// ============================================================================
	// DynamicPositionAttribute::read
	// ============================================================================
	void DynamicPositionAttribute::read (Tango::DeviceImpl *, Tango::Attribute &attr)
	{
		DEBUG_STREAM << "DynamicPositionAttribute::read " 
			<< conf.m_device_name 
			<< ":" 
			<< conf.m_attr_name 
			<< std::endl;
		double pos = __NAN__;
		Tango::DeviceAttribute da;
		try
		{
			da = dp->read_attribute(conf.m_attr_name);  
			da >> pos;
		}
		catch (...)
		{
			ERROR_STREAM << "DynamicPositionAttribute::read failed to get "
				<< conf.m_device_name 
				<< ":" 
				<< conf.m_attr_name 
				<< std::endl;
		}

		attr.set_value(& pos);
	}

	// ============================================================================
	// DynamicPositionAttribute::write
	// ============================================================================
	void DynamicPositionAttribute::write (Tango::DeviceImpl *, Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "DynamicPositiohAttribute::write " 
			<< conf.m_device_name 
			<< ":" 
			<< conf.m_attr_name 
			<< std::endl;
		Tango::DevDouble src = __NAN__;
		Tango::DeviceAttribute da;
		try
		{
			attr.get_write_value(src);
			Tango::DeviceAttribute da(conf.m_attr_name, src);
			dp->write_attribute(da);  
		}
		catch (...)
		{
			ERROR_STREAM << "DynamicPositionAttribute::write failed to write "
				<< conf.m_device_name 
				<< ":" 
				<< conf.m_attr_name 
				<< std::endl;
		} 
	}

	// ============================================================================
	// Config::Config
	// ============================================================================
	DynamicPositionAttribute::Config::Config ()
	{
		name           = "not initialized";
		m_device_name  = "not initialized";
		m_attr_name    = "not initialized";
		m_rw           = Tango::READ_WRITE;
		label          = "not initialized";
		description    = "not initialized";
		m_host         = 0;
	}

	DynamicPositionAttribute::Config::Config (const Config & _src)
	{
		*this = _src;
	}
	// ============================================================================
	// Config::operator =
	// ============================================================================
	void DynamicPositionAttribute::Config::operator = (const Config & _src)
	{
		name           = _src.name;
		m_device_name  = _src.m_device_name;
		m_attr_name    = _src.m_attr_name;
		m_rw           = _src.m_rw;
		label          = _src.label;
		description    = _src.description;
		m_host         = _src.m_host;
	}
} //- namespace smd

